
let charCount = function (charParam) {
    let obj = {};
    let i;
    let str = charParam.toLowerCase();

    for (i = 0; i < str.length; i++) {
        let char = str[i];

        if (isAlphanumeric(char)) {
            if (obj[char] > 0) {
                obj[char]++;
            } else {
                obj[char] = 1;
            }
        }
    }
    // console.log(obj);
    return obj;
}

let isAlphanumeric = function(char) {
    const charCode = char.charCodeAt();
    if (!(charCode > 47 && charCode < 58) &&
        !(charCode > 64 && charCode < 91) &&
        !(charCode > 96 && charCode < 123)) {
            return false;
    }
    return true;
}

charCount('This is a test')
