function validAnagram(word1, word2) {
    if (word1.length !== word2.length) {
        return false;
    }
    
    let lookup = {};

    for (let char of word1) {
        if (lookup[char] > 0) {
            lookup[char]++;
        } else {
            lookup[char] = 1;
        }
    }

    for (let char of word2) {
        // Can't find it
        if (!lookup[char]) {
            return false;
        } else {
            lookup[char]--;
        }
    }
    // valid
    return true;
}
validAnagram('word11','word1');